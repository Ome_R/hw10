#pragma once
#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

class Item
{
public:
	Item(string, string, double);
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	string getName() const;
	string getSerialNumber() const;
	int getCount() const;
	double getPrice() const;
	void setCount(const int count) const;
private:
	string _name;
	string _serialNumber; //consists of 5 numbers
	mutable int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};