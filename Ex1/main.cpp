#include "Customer.h"
#include <map>
#include <string>
#include <Windows.h>

using std::cout;
using std::cin;
using std::endl;
using std::pair;

#define MAIN_MENU "Welcome to MagshiMart!\n1. to sign as customer and buy items\n2. to update existing customer's items\n3. to print the customer who pays the most\n4. to exit\nWhat would you like to do? (1-4): "
#define ITEMS_MENU "Please choose an item from the list below (0 to exit):\n1. Milk $5.3\n2. Cookies $12.6\n3. Bread $8.9\n4. Chocolate $7\n5. Cheese $15.3\n6. Rice $6.2\n7. Fish $31.65\n8. Chicken $25.99\n9. Cucumber $1.21\n10. Tomato $2.32"
#define UPDATE_MENU "What would you like to do?\n1. Add items\n2. Remove item\n3. Back to menu\nWhat do you want to do next? "

#define SIGN_UP_OPTION 1
#define UPDATE_OPTION 2
#define MOST_OPTION 3
#define CLOSE_OPTION 4

#define ADD_ITEM_ACTION 0
#define REMOVE_ITEM_ACTION 1
#define BACK_MENU_ACTION 2

typedef map<string, Customer> Map;
typedef pair<string, Customer> Pair;

int getMainMenuOption();
void handleSignupProcess(Item itemList[], Map * abcCustomers);
void handleUpdateProcess(Item itemList[], Map * abcCustomers);
void printCustomerWhoPaidMost(Map abcCustomers);
void manageCustomerItems(Item itemList[], Customer& customer, int action);

int main()
{
	// Initializing customer map & items list
	Map abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("Bread","00003",8.9),
		Item("Chocolate","00004",7.0),
		Item("Cheese","00005",15.3),
		Item("Rice","00006",6.2),
		Item("Fish", "00008", 31.65),
		Item("Chicken","00007",25.99),
		Item("Cucumber","00009",1.21),
		Item("Tomato","00010",2.32)
	};

	// Get the action of the user from the main menu
	int userOption = CLOSE_OPTION;

	while ((userOption = getMainMenuOption()) != CLOSE_OPTION) {

		system("cls");
		
		switch (userOption) {
			case SIGN_UP_OPTION:
				handleSignupProcess(itemList, &abcCustomers);
				break;
			case UPDATE_OPTION:
				handleUpdateProcess(itemList, &abcCustomers);
				break;
			case MOST_OPTION:
				printCustomerWhoPaidMost(abcCustomers);
				break;
		}

		Sleep(1500);
		system("cls");
	}

	system("pause");

	return 0;
}

/*Function that returns the action of the user from the main menu.*/
int getMainMenuOption() {
	int userOption = 4;

	cout << MAIN_MENU;
	cin >> userOption;

	return userOption;
}

/*Function that handles the signup process.*/
void handleSignupProcess(Item itemList[], Map * abcCustomers) {
	// Get the name of the customer
	string customerName;
	cout << "Please enter your name: ";
	cin >> customerName;

	// Creating the customer instance
	Customer customer(customerName);
	// Adding the customer instance to the customers map.
	abcCustomers->insert(Pair(customerName, customer));
	// Opening items menu and add items to the customer.
	manageCustomerItems(itemList, abcCustomers->find(customerName)->second, ADD_ITEM_ACTION);
}

/*Function that handles the update process.*/
void handleUpdateProcess(Item itemList[], Map * abcCustomers) {
	// Getting the customer name
	string customerName;
	cout << "Please enter your name: ";
	cin >> customerName;

	// Getting the customer instance from the map
	Map::iterator it = abcCustomers->find(customerName);

	// Checks if a valid name was entered
	if (it == abcCustomers->end()) {
		cout << "Couldn't find a customer with that name." << endl;
	}
	else {
		it->second.printItems();
		// Printing update menu & getting the user action
		int action = 2;
		cout << UPDATE_MENU;
		cin >> action;
		// Processing users action
		if(BACK_MENU_ACTION != action - 1)
			manageCustomerItems(itemList, it->second, action - 1);
	}

}

/*Function that handles the search for the customer who paid the most.*/
void printCustomerWhoPaidMost(Map abcCustomers) {
	string customerName = "";
	double totalSum = 0;

	for (Map::iterator iter = abcCustomers.begin(); iter != abcCustomers.end(); iter++) {
		// Checking if the customer has paid more than totalSum.
		if (iter->second.totalSum() > totalSum) {
			// Swaping
			totalSum = iter->second.totalSum();
			customerName = iter->second.getName();
		}
	}

	if (customerName == "")
		cout << "No one has bought anything yet." << endl;
	else
		cout << "The customer who paid the most is " << customerName << " [$" << totalSum << "]" << endl;
}

/*Function that handles items menu actions.*/
void manageCustomerItems(Item itemList[], Customer& customer, int action) {
	//Printing items menu.
	cout << ITEMS_MENU << endl;
	int index = 0;

	cout << "What item would you like to choose? ";

	// We want to use a loop - we should do the same process until 0 is entered.
	do{
		//Getting the item from the user
		cin >> index;

		//Fixing the index to be corret
		index--;

		//Making sure it's a valid index (between 0 and 9)
		if (index >= 0 && index <= 9) {
			switch (action) {
				case ADD_ITEM_ACTION:
					customer.addItem(itemList[index]);
					cout << "What item would you like to choose? ";
					break;
				case REMOVE_ITEM_ACTION:
					customer.removeItem(itemList[index]);
					cout << "What item would you like to choose? ";
					break;
				default:
					return;
			}
		}
		//If it's not a stop index (-1), we want to tell the user about an incorrect item.
		else if (index != -1) {
			cout << "Invalid item. Please try again: ";
		}
	} while (index != -1);
}