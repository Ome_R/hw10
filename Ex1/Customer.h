#pragma once
#include "Item.h"
#include <set>

typedef set<Item> Set;

class Customer
{
public:
	Customer(string);
	Customer();
	double totalSum() const;//returns the total sum for payment
	void printItems() const;
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	string getName() const;
private:
	string _name;
	Set _items;

};
