#include "Customer.h"

Customer::Customer(string name)
{
	_name = name;
}

Customer::Customer()
{
	std::free(&_items);
}

double Customer::totalSum() const
{
	double sum = 0;

	for (Item item : _items) {
		sum += item.totalPrice();
	}

	return sum;
}

void Customer::printItems() const
{
	for (Item item : _items) {
		cout << " - x" << item.getCount() << " " << item.getName() << " [$" << item.totalPrice() << "]" << endl;
	}
}

void Customer::addItem(Item item)
{
	Set::iterator it = _items.find(item);
	if (it == _items.end()) {
		_items.insert(item);
		cout << "Added this item to your shopping cart." << endl;
	}
	else {
		it->setCount(it->getCount() + 1);
		cout << "Updated the amount of this item to " << it->getCount() << endl;
	}

}

void Customer::removeItem(Item item)
{
	Set::iterator it = _items.find(item);
	if (it == _items.end()) {
		cout << "Couldn't find this item in your shopping cart." << endl;
	}
	else {
		if (it->getCount() <= 1) {
			_items.erase(item);
			cout << "Removed this item from your shopping cart." << endl;
		}
		else {
			it->setCount(it->getCount() - 1);
			cout << "Updated the amount of this item to " << it->getCount() << endl;
		}
	}
}

string Customer::getName() const
{
	return _name;
}
