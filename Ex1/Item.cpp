#include "Item.h"

Item::Item(string name, string serialNumber, double price)
{
	_name = name;
	_serialNumber = serialNumber;
	_count = 1;
	_unitPrice = price;
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item & other) const
{
	return _serialNumber < other._serialNumber;
}

bool Item::operator>(const Item & other) const
{
	return !operator<(other) && !operator==(other);
}

bool Item::operator==(const Item & other) const
{
	return _serialNumber == other._serialNumber;
}

string Item::getName() const
{
	return _name;
}

string Item::getSerialNumber() const
{
	return _serialNumber;
}

int Item::getCount() const
{
	return _count;
}

double Item::getPrice() const
{
	return _unitPrice;
}

void Item::setCount(const int count) const
{
	if (count < 1) {
		_count = 1;
	}
	else {
		_count = count;
	}
}
